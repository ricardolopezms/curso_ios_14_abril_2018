//
//  ViewController.swift
//  CustomTableViewRepaso
//
//  Created by Ricardo López on 12/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var dataSource: [WeatherModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let weatherMexico = WeatherModel(city: "México", grades: "32º C", cityImage: "mexico", weatherImage: "sunny")
        let weatherParis = WeatherModel(city: "París", grades: "12º C", cityImage: "paris", weatherImage: "windy")
        let weatherNewYork = WeatherModel(city: "New York", grades: "22º C", cityImage: "newyork", weatherImage: "cloudy")
        
        dataSource.append(weatherMexico)
        dataSource.append(weatherParis)
        dataSource.append(weatherNewYork)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherTableViewCell") as! WeatherTableViewCell
        cell.weatherModel = dataSource[indexPath.row]
        return cell
    }
    
}

