//
//  WeatherModel.swift
//  CustomTableViewRepaso
//
//  Created by Ricardo López on 12/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class WeatherModel {
    
    var city = ""
    var grades = ""
    var cityImage = ""
    var weatherImage = ""
    
    init(city: String, grades: String, cityImage: String, weatherImage: String) {
        self.city = city
        self.grades = grades
        self.cityImage = cityImage
        self.weatherImage = weatherImage
    }
}
