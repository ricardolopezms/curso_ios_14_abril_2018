//
//  WeatherTableViewCell.swift
//  CustomTableViewRepaso
//
//  Created by Ricardo López on 12/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {

    @IBOutlet weak var myContainerView: UIView!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var gradesLabel: UILabel!
    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var weatherImageView: UIImageView!
    
    var weatherModel: WeatherModel! {
        didSet {
            configureCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        myContainerView.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: - Private methods
    
    private func configureCell() {
        countryLabel.text = weatherModel.city
        gradesLabel.text = weatherModel.grades
        weatherImageView.image = UIImage(named: weatherModel.weatherImage)
        countryImageView.image = UIImage(named: weatherModel.cityImage)
    }
}
