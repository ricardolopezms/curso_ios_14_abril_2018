//
//  VisualFormatLanguageViewController.swift
//  ScrollViewExample
//
//  Created by Ricardo López on 28/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class VisualFormatLanguageViewController: UIViewController {

    @IBOutlet weak var contentScrollView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        /*
        //let redView = UIView(frame: CGRect(x: 0, y: 0, width: 1000, height: 1000))
        let redView = UIView()
        redView.backgroundColor = .red
        contentScrollView.addSubview(redView)
        
        redView.translatesAutoresizingMaskIntoConstraints = false
        
        let horizontalFormat = "H:|-[redView]-|"
        let verticalFormat = "V:|-[redView(1000)]-|"
        
        let views = [
            "redView" : redView
        ]
        
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: horizontalFormat, options: .alignAllFirstBaseline, metrics: nil, views: views)
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: verticalFormat, options: .alignAllFirstBaseline, metrics: nil, views: views)
        
        contentScrollView.addConstraints(horizontalConstraints)
        contentScrollView.addConstraints(verticalConstraints)
         */
        
        createViews(numberOfViews: 10)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Private methods
    
    private func createViews(numberOfViews: Int) {
        
        var verticalViews: [String: UIView] = [:]
        var verticalElements: [String] = []
        
        for x in 0..<numberOfViews {
            
            // Creamos un view y lo ponemos en el contentScrollView
            let view = UIView()
            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            contentScrollView.addSubview(view)
            
            // Creamos un identificador único para el view
            let viewString = "view\(x)"
            
            // Agregamos el identificador al arreglo de elementos
            verticalElements.append("[\(viewString)(ALTURA)]")
            
            //Agregamos el view al diccionario para referencia en las constraints
            verticalViews[viewString] = view
            
            // Creamos el formato de constraints horizontal
            let horizontalFormat = "H:|-[\(viewString)]-|"
            
            // Agregamos las constraints horizontales
            let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: horizontalFormat, options: .alignAllFirstBaseline, metrics: nil, views: verticalViews)
            
            // Agregamos las constraints al Scroll
            contentScrollView.addConstraints(horizontalConstraints)
        }
        
        //print(verticalElements)
        // Creamos el formato de constraints vertical
        let verticalFormat = "V:|-\(verticalElements.joined(separator: "-"))-|"
        print(verticalFormat)
        
        let metrics = [
            "ALTURA" : 100
        ]
        
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: verticalFormat, options: .alignAllLeft, metrics: metrics, views: verticalViews)
        contentScrollView.addConstraints(verticalConstraints)
    }

}







