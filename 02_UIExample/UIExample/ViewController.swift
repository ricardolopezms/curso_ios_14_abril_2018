//
//  ViewController.swift
//  UIExample
//
//  Created by Ricardo López on 14/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var helloWorldLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        helloWorldLabel.text = "Hola mundo cruel"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - User interaction
    
    @IBAction func leftButtonPressed(_ sender: Any) {
        //print("Left!")
        helloWorldLabel.textAlignment = .left
    }
    
    @IBAction func centerButtonPressed(_ sender: Any) {
        helloWorldLabel.textAlignment = .center
    }
    
    @IBAction func rightButtonPressed(_ sender: Any) {
        helloWorldLabel.textAlignment = .right
    }
    
    @IBAction func redButtonPressed(_ sender: Any) {
        helloWorldLabel.textColor = .red
    }
    
    @IBAction func blackButtonPressed(_ sender: Any) {
        helloWorldLabel.textColor = .black
    }
    
    @IBAction func blueButtonPressed(_ sender: Any) {
        helloWorldLabel.textColor = .blue
    }
    
    @IBAction func sliderChanged(_ sender: UISlider) {
        //print(sender.value)
        imageView.alpha = CGFloat(sender.value)
    }
    
}









