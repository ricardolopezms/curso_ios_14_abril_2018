//
//  CreateTodoViewController.swift
//  TodoListExample
//
//  Created by Ricardo López on 19/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

/*
 1) Declaramos nuestro protocolo
 2) Declaramos una variable llamada 'delegate' del tipo del protocolo en la clase desde donde vamos a enviar datos
 3) Implementar el protocolo en la clase en la que vamos a recibir los datos
 4) Asiganamos la variable 'delegate' en el prepareForSegue de la clase que va a recibir los datos
 5) Mandamos los datos a traves de la variable 'delegate' desde la clase que los contiene
 */

// 1
protocol CreateTodoDelegate: class {
    func createTodoDidCreate(todoItem: TodoItem)
}

class CreateTodoViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var notesTextField: UITextField!
    @IBOutlet weak var colorButton: UIButton!
    @IBOutlet weak var createButtonBottomConstraint: NSLayoutConstraint!
    
    // 2
    weak var delegate: CreateTodoDelegate?
    
    private var selectedColor = UIColor.lightGray
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardDidHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SelectColorViewController" {
            let selectColorViewController = segue.destination as! SelectColorViewController
            selectColorViewController.delegate = self
            view.endEditing(true)
        }
    }
    
    // MARK: - User interaction
    
    @IBAction func createTodoItemButtonPressed(_ sender: Any) {
        guard let title = titleTextField.text, !title.isEmpty else { return }
        guard let notes = notesTextField.text else { return }
        
        let todoItem = TodoItem(title: title, notes: notes, done: false, color: selectedColor)
        print("TodoItem: \(todoItem.title)")
        // 5
        delegate?.createTodoDidCreate(todoItem: todoItem)
    }
    
    // MARK: - Private methods
    
    @objc private func keyboardWillShow(notification: Notification) {
        let userInfo = notification.userInfo!
        let keyboardSize = userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardFrame = keyboardSize.cgRectValue
        
        createButtonBottomConstraint.constant = keyboardFrame.height
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc private func keyboardWillHide(notification: Notification) {
        createButtonBottomConstraint.constant = 0
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
}

// MARK: - SelectColorDelegate

extension CreateTodoViewController: SelectColorDelegate {
    
    func selectColorDidSelect(color: UIColor) {
        selectedColor = color
        colorButton.backgroundColor = color
        navigationController?.popViewController(animated: true) 
    }
}



