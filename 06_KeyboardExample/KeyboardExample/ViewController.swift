//
//  ViewController.swift
//  KeyboardExample
//
//  Created by MobileStudio Laptop003 on 21/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet var nameTexField: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    @IBOutlet var ageTextField: UITextField!
    @IBOutlet var emailTextFied: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    private var currentTextField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTexField.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Set Keyboard Notifications
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //view.endEditing(true)
        currentTextField?.resignFirstResponder()
    }
    
    // MARK: - Keyboard Functions
    
    @objc func keyboardWillShow(notification: Notification) {
        print("keyboardWillShow")
        // Obtenemos el tamaño del teclado para modificar nuestra contraint
        let userInfo = notification.userInfo!
        print(userInfo)
        let keyboardSize = userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardFrame = keyboardSize.cgRectValue
        
        let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        bottomConstraint.constant = 10 + keyboardFrame.height
        
        // Animamos la transición de los textField
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        print("keyboardWillHide")
        let userInfo = notification.userInfo!
        let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        // Regresamos el formulario a su posición original
        bottomConstraint.constant = 10
        
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
}

extension ViewController: UITextFieldDelegate {
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("ShouldClear in \(textField)")
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTexField:
            lastNameTextField.becomeFirstResponder()
        case lastNameTextField:
            ageTextField.becomeFirstResponder()
        case emailTextFied:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
//            self.view.endEditing(true)
            currentTextField?.resignFirstResponder()
        default:
            break
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        currentTextField = nil
    }
}

