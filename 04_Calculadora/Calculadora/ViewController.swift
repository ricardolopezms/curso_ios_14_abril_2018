//
//  ViewController.swift
//  Calculadora
//
//  Created by Ricardo López on 14/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var displayLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - User interaction
    
    @IBAction func numberButtonPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func operationButtonPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func clearButtonPressed(_ sender: Any) {
        
    }
}

