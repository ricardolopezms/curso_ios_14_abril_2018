//
//  CatalogModel.swift
//  ParserExample
//
//  Created by Ricardo López on 26/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class CatalogModel: Codable {
    
    var books: [Book]
}
