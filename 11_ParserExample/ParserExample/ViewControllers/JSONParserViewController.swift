//
//  JSONParserViewController.swift
//  ParserExample
//
//  Created by Ricardo López on 19/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import Alamofire

class JSONParserViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var dataSource: [Book] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "JSON Parser"
        loadBooks()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BookDetailViewController" {
            let bookDetailViewController = segue.destination as! BookDetailViewController
            bookDetailViewController.book = sender as! Book
        }
    }
    
    // MARK: - Private methods
    
    private func loadBooks() {
        
        Alamofire.request(Constants.API.jsonURLStr)
            .validate()
            .responseData { (response) in
                
                switch response.result {
                case .success(let value):
                    print("JSON: \(value)")
                    do {
                        let booksResponse = try JSONDecoder().decode(BooksResponseModel.self, from: value)
                        print("Libros parseados: \(booksResponse.catalog.books.count)")
                        self.dataSource = booksResponse.catalog.books
                        
                        self.tableView.reloadData()
                    }
                    catch let error {
                        print("Error: \(error.localizedDescription)")
                    }
                    
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                }
        }
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension JSONParserViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "JSONBookTableViewCell") as! JSONBookTableViewCell
        cell.book = dataSource[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let book = dataSource[indexPath.row]
        performSegue(withIdentifier: "BookDetailViewController", sender: book)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}









