//
//  XMLParserViewController.swift
//  ParserExample
//
//  Created by Ricardo López on 19/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import Alamofire

class XMLParserViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var dataSource: [Book] = []
    private var auxBook: Book!
    private var auxString: String!
    private var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "XML Parser"
        refreshControl.addTarget(self, action: #selector(loadBooks), for: .valueChanged)
        tableView.addSubview(refreshControl)
        loadBooks()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BookDetailViewController" {
            let bookDetailViewController = segue.destination as! BookDetailViewController
            bookDetailViewController.book = sender as! Book
        }
    }
    
    // MARK: - Private methos
    
    @objc private func loadBooks() {
        Alamofire.request(Constants.API.xmlURLStr)
        .validate()
            .responseData { (response) in
                switch response.result {
                case .success(let value):
                    
                    self.dataSource.removeAll()
                    
                    print("DATA: \(value)")
                    let parser = XMLParser(data: value)
                    parser.delegate = self
                    parser.parse()
                    print("Libros parseados: \(self.dataSource.count)")
                    
                    for book in self.dataSource {
                        print(book.title)
                        print("\t\(book.id)")
                        print("\t\(book.author)")
                        print("\t\(book.genre)")
                        print("\t\(book.price)")
                        print("\t\(book.publishDate)")
                        print("\t\(book.bookDescription)")
                    }
                    
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()
                    
                case .failure(let error):
                    print("XML Error: \(error.localizedDescription)")
                }
        }
    }
}

// MARK: - XMLParserDelegate

extension XMLParserViewController: XMLParserDelegate {
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        //print("ABRE: \(elementName)")
        if elementName == "book" {
            auxBook = Book()
            if let bookId = attributeDict["id"] {
                auxBook.id = bookId
            }
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        //print("Caracteres encontrados...")
        auxString = string
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        //print("CIERRA: \(elementName)")
        switch elementName {
        case "author":
            auxBook.author = auxString
        case "genre":
            auxBook.genre = auxString
        case "title":
            auxBook.title = auxString
        case "price":
            auxBook.price = auxString
        case "publish_date":
            auxBook.publishDate = auxString
        case "description":
            auxBook.bookDescription = auxString
        case "book":
            dataSource.append(auxBook)
        default:
            print("Nothing to do here :(")
        }
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension XMLParserViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "XMLBookTableViewCell") as! XMLBookTableViewCell
        cell.book = dataSource[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let book = dataSource[indexPath.row]
        performSegue(withIdentifier: "BookDetailViewController", sender: book)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}



