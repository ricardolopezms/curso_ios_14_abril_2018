//
//  PlanetDetailViewController.swift
//  TableViewExample
//
//  Created by Ricardo López on 05/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class PlanetDetailViewController: UIViewController {
    
    @IBOutlet weak var planetNameLabel: UILabel!
    @IBOutlet weak var planetDescriptionLabel: UILabel!
    
    var planet: Planet!

    override func viewDidLoad() {
        super.viewDidLoad()

        print("Planeta recibido: \(planet.name)")
        planetNameLabel.text = planet.name
        planetDescriptionLabel.text = planet.planetDescription
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
