//
//  ColorsViewController.swift
//  TableViewExample
//
//  Created by Ricardo López on 05/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ColorsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var colorsArray: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        colorsArray.append("Azul")
        colorsArray.append("Negro")
        colorsArray.append("Rojo")
        colorsArray.append("Verde")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension ColorsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return colorsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ColorTableViewCell")!
        cell.textLabel?.text = colorsArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected: \(colorsArray[indexPath.row])")
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
