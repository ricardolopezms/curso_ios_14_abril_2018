//
//  MainViewController.swift
//  TableViewExample
//
//  Created by Ricardo López on 05/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

/*
 1) Hacemos un outlet de nuestro tableView
 2) Conectamos el dataSource y el delegate
 3) Adotamos los protocolos en el ViewController
 4) Agregar una celda y darle un identificador
 */

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    private var planetsArray: [Planet] = []
    private var studentsArray: [Student] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Planetas
        planetsArray.append(Planet(name: "Mercurio", planetDescription: "Descripción de Mercurio"))
        planetsArray.append(Planet(name: "Venus", planetDescription: "Descripción de Venus"))
        planetsArray.append(Planet(name: "Tierra", planetDescription: "Descripción de Tierra"))
        planetsArray.append(Planet(name: "Marte", planetDescription: "Descripción de Marte"))
        planetsArray.append(Planet(name: "Júpiter", planetDescription: "Descripción de Júpiter"))
        planetsArray.append(Planet(name: "Saturno", planetDescription: "Descripción de Saturno"))
        planetsArray.append(Planet(name: "Urano", planetDescription: "Descripción de Urano"))
        planetsArray.append(Planet(name: "Neptuno", planetDescription: "Descripción de Neptuno"))
        planetsArray.append(Planet(name: "Plutón", planetDescription: "Descripción de Plutón"))
        
        // Estudiantes
        studentsArray.append(Student(name: "Ricardo López", programmingLanguage: "Swift 1.2"))
        studentsArray.append(Student(name: "Cesar Guzmán", programmingLanguage: "Swift 2.3"))
        studentsArray.append(Student(name: "Josimar Revelo", programmingLanguage: "Swift 3"))
        studentsArray.append(Student(name: "Edgar Santiago", programmingLanguage: "Swift 4.1"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PlanetDetailViewController" {
            let planetDetailViewController = segue.destination as! PlanetDetailViewController
            planetDetailViewController.planet = sender as! Planet
        }
    }
    
    // MARK: - User interaction
    
    @IBAction func editButtonPressed(_ sender: Any) {
        tableView.setEditing(!tableView.isEditing, animated: true)
        editButton.title = tableView.isEditing ? "Listo" : "Editar"
    }
    
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Planetas"
        }
        return "Estudiantes"
    }
    
    // Este métodos nos indica cuántas celdas se van a mostrar en la sección dada
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return planetsArray.count
        }
        return studentsArray.count
    }
    
    // Este método nos crea la UI de la celda que se va a mostrar en el indexPath dado
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlanetTableViewCell")!
            let planet = planetsArray[indexPath.row]
            cell.textLabel?.text = planet.name
            cell.accessoryType = .disclosureIndicator
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StudentTableViewCell")!
            let student = studentsArray[indexPath.row]
            cell.textLabel?.text = student.name
            cell.detailTextLabel?.text = student.programmingLanguage
            cell.accessoryType = .checkmark
            return cell
        }
    }
    
    // Este método nos comunica cuando el usuario da click en la celda
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected \(indexPath)")
        if indexPath.section == 0 {
            let planet = planetsArray[indexPath.row]
            performSegue(withIdentifier: "PlanetDetailViewController", sender: planet)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // Este método nos dice qué celdas se pueden editar y qué celdas no
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 {
            return false
        }
        return true
    }
    
    // Este método se llama cuando el usuario realiza una acción de edición
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Eliminamos el estudiante del arreglo y eliminamos la celda de la tabla
            studentsArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "😱"
    }
    
    // Este método nos dice si podemos mover la celda en el indexPath dado o no
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // Este método nos dice de dónde a dónde se está moviendo la celda
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let studentToMove = studentsArray[sourceIndexPath.row]
        studentsArray.remove(at: sourceIndexPath.row)
        studentsArray.insert(studentToMove, at: destinationIndexPath.row)
    }
    
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        if sourceIndexPath.section == proposedDestinationIndexPath.section {
            return proposedDestinationIndexPath
        }
        return sourceIndexPath
    }
}

















