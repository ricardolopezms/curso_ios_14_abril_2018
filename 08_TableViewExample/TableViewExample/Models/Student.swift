//
//  Student.swift
//  TableViewExample
//
//  Created by Ricardo López on 05/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class Student {
    var name = ""
    var programmingLanguage = ""
    
    init(name: String, programmingLanguage: String) {
        self.name = name
        self.programmingLanguage = programmingLanguage
    }
}
