//
//  Planet.swift
//  TableViewExample
//
//  Created by Ricardo López on 05/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class Planet {
    
    var name = ""
    var planetDescription = ""
    
    init(name: String, planetDescription: String) {
        self.name = name
        self.planetDescription = planetDescription
    }
}
