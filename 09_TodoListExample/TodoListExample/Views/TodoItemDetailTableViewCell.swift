//
//  TodoItemDetailTableViewCell.swift
//  TodoListExample
//
//  Created by Ricardo López on 12/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class TodoItemDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var todoItemTitleLabel: UILabel!
    @IBOutlet weak var todoItemNotesLabel: UILabel!
    @IBOutlet weak var colorIndicatorView: UIView!
    
    var todoItem: TodoItem! {
        didSet {
            configureCell()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Private methods
    
    private func configureCell() {
        if todoItem.done {
            colorIndicatorView.backgroundColor = .green
            let attributesDict: [NSAttributedStringKey: Any] = [
                NSAttributedStringKey.strikethroughStyle : 1
            ]
            let attributedString = NSAttributedString(string: todoItem.title, attributes: attributesDict)
            todoItemTitleLabel.attributedText = attributedString
        }
        else {
            colorIndicatorView.backgroundColor = .red
            todoItemTitleLabel.text = todoItem.title
        }
        todoItemNotesLabel.text = todoItem.notes
    }
}
