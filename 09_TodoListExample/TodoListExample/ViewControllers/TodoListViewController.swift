//
//  TodoListViewController.swift
//  TodoListExample
//
//  Created by Ricardo López on 05/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class TodoListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var dataSource: [TodoItem] = []
    private var isDetailActive = false

    override func viewDidLoad() {
        super.viewDidLoad()
    
        //createDummyData()
        if let todosDict = readJsonToDict(fileName: "todos.json") {
            // print("Diccionario: \(todosDict)")
            // Diccionario -> [String: Any]
            // Arreglo de enteros -> [Int]
            // Arreglo de diccionarios -> [[String: Any]]
            if let todosArray = todosDict["todos"] as? [[String: Any]] {
                print("Items: \(todosArray.count)")
                
                for todoDict in todosArray {
                    if let title = todoDict["title"] as? String, let notes = todoDict["notes"] as? String, let done = todoDict["done"] as? Bool {
                        let todoItem = TodoItem(title: title, notes: notes, done: done)
                        print("Todo creado")
                        dataSource.append(todoItem)
                    }
                }
                print("Datasource: \(dataSource)")
            }
        }
        else {
            print("El archivo no existe :(")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isDetailActive = UserDefaults.standard.bool(forKey: "DETAIL_ACTIVE")
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Private methods
    
    private func createDummyData() {
        
        let todoDict: [String: Any] = [
            "title" : "Estudiar Swift",
            "notes" : "Buscar ejemplos de Swift y entenderlos",
            "done" : false
        ]
        
        let todoDict2: [String: Any] = [
            "title" : "Comprar leche",
            "notes" : "Deslactosada porque me hace daño la entera",
            "done" : false
        ]
        
        let todoDict3: [String: Any] = [
            "title" : "Lavar ropa",
            "notes" : "Ya no tengo ropa limpia",
            "done" : true
        ]
        
        let todos = [
            "todos" : [todoDict, todoDict2, todoDict3]
        ]
        
        writeDictToJson(dict: todos, fileName: "todos.json")
    }
    
    private func writeDictToJson(dict: [String: Any], fileName: String) {
        let data = try! JSONSerialization.data(withJSONObject: dict, options: [.prettyPrinted])
        let jsonString = String(data: data, encoding: .utf8)
        //print(jsonString)
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        //print(paths)
        let completePath = paths[0].appending("/\(fileName)")
        print(completePath)
        
        do {
            try jsonString?.write(toFile: completePath, atomically: true, encoding: .utf8)
        }
        catch let error {
            print("Save file error: \(error.localizedDescription)")
        }
    }
    
    private func readJsonToDict(fileName: String) -> [String: Any]? {
        
        // Obtenemos el directorio de documentos
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        // Creamos la ruta completa del archivo
        let completePath = paths[0].appending("/\(fileName)")
        
        // Creamos la variable en donde vamos a vaciar el contenido del archivo
        var dict: [String: Any]?
        
        // Intentamos leer el archivo
        do {
            let jsonString = try String(contentsOfFile: completePath, encoding: .utf8)
            // print("JSON String: \(jsonString)")
            // Transformamos nuestro String a data y posteriormente a un JSON Object
            if let data = jsonString.data(using: .utf8) {
                dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            }
        }
        catch let error {
            print("Reading error: \(error.localizedDescription)")
        }
        
        return dict
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension TodoListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isDetailActive {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TodoItemDetailTableViewCell") as! TodoItemDetailTableViewCell
            cell.todoItem = dataSource[indexPath.row]
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TodoItemTableViewCell") as! TodoItemTableViewCell
            cell.todoItem = dataSource[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


















