//
//  ViewController.swift
//  BasicNavigation
//
//  Created by Ricardo López on 28/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var myView: UIView!
    
    override func loadView() {
        super.loadView()
        print("loadView")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear")
        if myView == nil {
            myView = UIView()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear")
        print(myView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print("didReceiveMemoryWarning")
        myView = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("viewDidDisappear")
    }

    // MARK: - User interaction
    
    @IBAction func showViewControllerProgrammaticallyButtonPressed(_ sender: Any) {
        
        // 1) Debemos conectar el segue al ViewController y no al botón
        // 2) Debemos asignar un identificardor al SEGUE para poder ejecutarlo desde código
        // 3) Mandamos llamar el segue
        
        performSegue(withIdentifier: "RedViewController", sender: self)
    }
    
    @IBAction func showViewControllerWithIdButtonPressed(_ sender: Any) {
        
        // 1) Debemos asignar un identificador al VIEWCONTROLLER
        // 2) Debemos crear el ViewController desde el Storyboard para poder presentarlo
        // 3) Presentamos el ViewController a través del NavigationController
        
        if let grayViewController = storyboard?.instantiateViewController(withIdentifier: "GrayViewController") {
            navigationController?.show(grayViewController, sender: nil)
        }

        // Para pasar a un ViewController que está en otro Storyboard desde códigoºx
//        let secondStoryboard = UIStoryboard(name: "Second", bundle: nil)
//        let blueViewController = secondStoryboard.instantiateViewController(withIdentifier: "BlueViewController")
//        navigationController?.show(blueViewController, sender: nil)
    }
    
    /*
     ShowViewController (Second Storyboard)
        1) Se crea un segundo Storyboard
        2) Se agrega un ViewController
        3) Se asigna un ID al ViewController al que queremos llegar desde el primer Storyboard
        4) Se agrega un StoryboardReference en el primer Storyboard
        5) Se le indica en el StoryboardReferece a que Storyboard y a que ID de ViewController hace referencia
    */
    
}









