//
//  ViewController.swift
//  ClasesYMetodos
//
//  Created by Ricardo López on 14/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        print("Hola mundo!")
        
        let x = 0                   // Constante implícita tipo Int
        let name = "Ricardo"        // Constante implícita tipo String
        
        let y: Int = 1                  // Constante explícita tipo Int
        let lastName: String = "López"  // Constante explícita tipo String
        
        print(x)
        print(name)
        print("\(x) - \(name)")
        print(String(x) + " - " + name)
        
        var x2 = 0
        var y2: Int = 2
        x2 = x2 + y2
        
        print("+++++ ARREGLOS +++++")
        
        let myArray = [1, 2, 3, 4, 5]
        let myArray2: [Int] = [1, 2, 3]
        let myArray3: Array<Int> = Array()
        let myArray4: [Int] = []
        let myArray5: [Any] = [1, 3.1416, "Hola"]
        
        print(myArray)
        print(myArray5)
        
        // For each
        for number in myArray {
            print("Número: \(number)")
        }
        
        // For tradicional
        for x in 0..<myArray.count {
            let number = myArray[x]
            print("Número tradicional: \(number)")
        }
        
        print("+++++ DICCIONARIOS +++++")
        
        let myDictionary = [
            "key" : "value",
            "name" : "Ricardo",
            "last_name" : "López"
        ]
        
        print(myDictionary)
        print(myDictionary["name"])
        print(myDictionary["name"]!)
        
        var myDictionary2: [String: Int] = [:]
        myDictionary2["uno"] = 1
        myDictionary2["dos"] = 2
        myDictionary2["tres"] = 3
        
        print(myDictionary2["dos"])
        print(myDictionary2["cuatro"])
        
        if let dos = myDictionary2["dos"] {
            print("Valor dos: \(dos)")
        }
        else {
            print("dos NO tiene valor")
        }
        
        if let cuatro = myDictionary2["cuatro"] {
            print("Valor cuatro: \(cuatro)")
        }
        else {
            print("cuatro NO tiene valor")
        }
        
        let stringOptional: String? = nil
        let stringOptionalTwo: String? = "Testing optionals"
        
        print(stringOptional)
        print(stringOptionalTwo)
        
        let keysArray = Array(myDictionary.keys)
        let valuessArray = Array(myDictionary.values)
        
        for key in keysArray {
            print("Key: \(key) -> value: \(myDictionary[key] ?? "No value")")
        }
        
        print("+++++ FUNCIONES +++++")
        
        sayHello()
        
        let result = suma(numA: 5, numB: 4)
        print("Result: \(result)")
        
        let resultTwo = sumaSinLabel(6, 6)
        print("ResultTwo: \(resultTwo)")
        
        let resultThree = sumaConLabelDiferente(x: 7, y: 7)
        print("ResultThree: \(resultThree)")
        
        let res = resta(numA: 5, numB: 3)
        let res2 = resta(numA: 10, numB: 4, numC: 2)
        
        print("RES: \(res)")
        print("RES2: \(res2)")
        
        let multiples = multiplesValores()
        print(multiples.0)
        print(multiples.1)
        print(multiples.2)
        
        let ricardo = multiplesValoresV2()
        print("Nombre: \(ricardo.name)")
        print("Edad: \(ricardo.age)")
        print("Altura: \(ricardo.height)m")
        
        
        let person = Person(name: "Ricardo López", age: 30)
        print(person.name)
        print(person.age)
        
        let developer = Developer(name: "Ricardo López", age: 30, programmingLanguage: "Swift")
        print(developer.name)
        print(developer.age)
        print(developer.programmingLanguage)
        
        person.sayHello()
        developer.sayHello()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Private methods
    
    private func sayHello() {
        print("Hello world func!")
    }
    
    private func suma(numA: Int, numB: Int) -> Int {
        return numA + numB
    }
    
    private func sumaSinLabel(_ numA: Int, _ numB: Int) -> Int {
        return numA + numB
    }
    
    private func sumaConLabelDiferente(x numA: Int, y numB: Int) -> Int {
        return numA + numB
    }
    
    private func resta(numA: Int, numB: Int, numC: Int? = nil) -> Int {
        if let numC = numC {
            return numA - numB - numC
        }
        return numA - numB
    }
    
    private func multiplesValores() -> (Int, String, Double) {
        return (25, "Hola mundo", 3.5)
    }
    
    private func multiplesValoresV2() -> (age: Int, name: String, height: Double) {
        return (30, "Ricardo", 1.67)
    }
}
















