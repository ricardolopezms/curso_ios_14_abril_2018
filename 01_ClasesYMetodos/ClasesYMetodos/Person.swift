//
//  Person.swift
//  ClasesYMetodos
//
//  Created by Ricardo López on 14/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class Person {
    
    var name = ""
    var age = 0
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
    
    func sayHello() {
        print("Hola, soy \(name) y tengo \(age) años")
    }
}
