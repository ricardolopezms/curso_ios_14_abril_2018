//
//  Developer.swift
//  ClasesYMetodos
//
//  Created by Ricardo López on 14/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class Developer: Person {
    
    var programmingLanguage = ""
    
    init(name: String, age: Int, programmingLanguage: String) {
        super.init(name: name, age: age)
        self.programmingLanguage = programmingLanguage
    }
    
    override func sayHello() {
        super.sayHello()
        print("Además soy developer y programo en \(programmingLanguage)")
    }
}
